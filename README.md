Many of the life skills that the players learn through soccer are beneficial in later life, how to form and build relationships, developing a sense of co-operation, how to lead people, how to handle adversity, what commitment means and punctuality, are all key qualities that will benefit the players in adult life.

If your child is already playing soccer, great. But what can you do if you want to get your child involved in a local soccer team? Here are seven must ask questions that you and your child should consider when attempting to find a suitable club...

1) What level of commitment is required in terms of practice sessions and games at the weekend?

2) When are the training sessions? When are games played, what's the format and how much time will you need to devote?

http://www.womencrossfitshoes.com/
